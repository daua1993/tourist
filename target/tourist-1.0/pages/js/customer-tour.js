/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tblExe = new TableExecute();

function rowCreatedCallBack(row, data, index) {
    $('td', row).eq(7).on('click', '.btn-success', function () {
        console.log("rowCreatedCallBack");
        var request = new Request();
        request.request("GetRefunTicketService", {"ticket_id": data.id}, function (data) {
            console.log("res: " + JSON.stringify(data));
            hideAllModal();

            var html = '';
            html += '<label>Mã tour :</label> ' + data.tour_id + ' <br/>';
            html += '<label>Tên tour :</label> ' + data.tour_name + ' <br/>';
            html += '<label>Người đại diện :</label> ' + data.deputation + ' <br/>';
            html += '<label>Thời gian khởi hành :</label> ' + data.start_time + ' <br/>';
            html += '<label>Điểm xuất phát :</label> ' + data.locationStart + ' <br/>';
            html += '<label>Điểm đến :</label> ' + data.locationEnd + ' <br/>';
            html += '<label>Số lượng khách hàng :</label> ' + data.numberOfGuess + ' <br/>';
            html += '<label>Tên khách hàng :</label> ' + data.customer_name + ' <br/>';
            html += '<label>SĐT :</label> ' + data.phone + ' <br/>';
            html += '<label>Giá tour :</label> ' + data.price.formatMoneyEx(0) + ' VND <br/>';
            html += '<label>Tiền trả lại :</label> ' + data.refun.formatMoneyEx(0) + ' VND <br/>';
            html += '<label>Tiền phạt :</label> ' + data.phat.formatMoneyEx(0) + ' VND <br/>';

            /*
             * {
             "tour_id": 1,
             "created_time": "Mar 10, 2019 6:00:00 AM",
             "updated_time": "Mar 10, 2019 6:00:00 AM",
             "tour_name": "HÀ NỘI - QUY NHƠN ",
             "deputation": "Thân Xuân Sơn",
             "locationStart": "Hà Nội",
             "locationEnd": "Quy Nhơn",
             "numberOfGuess": 10,
             "register_time": "Mar 4, 2019 6:00:00 AM",
             "start_time": "Mar 15, 2019 6:00:00 AM",
             "phone": "0366787321",
             "price": 15000000,
             "refun": 7500000,
             "id": 2,
             "customer_name": "Chu Thị Linh",
             "customer_id": 2,
             "status": 0,
             "phat": 7500000
             }
             */
            BootstrapDialog.show({
                title: '<b>Trả vé</b>',
                type: BootstrapDialog.TYPE_DEFAULT,
                size: BootstrapDialog.SIZE_WIDE,
                message: function (dialog) {
                    var $content = $(html);
                    return $content;
                },
                buttons: [
                    {
                        label: 'Trả vé',
                        cssClass: 'btn-success',
                        hotkey: 13,
                        action: function (dialogRef) {

                            confirmDialog("Bạn muốn trả vé này chứ", "Trả vé", function () {
                                var params = {
                                    "ticket_id": data.id,
                                };
                                console.log("params: " + JSON.stringify(params));
                                var req = new Request();
                                dialogRef.close();
                                req.postRequest("RefunService", params, function (rs) {
                                    hideAllModal();
                                    var rc = parseInt(rs.result);
                                    var rd = rs.detail;
                                    if (rc === 0) {
                                        showDialog("Thông báo", "Trả vé thành công", null, function () {
                                            window.location.href = "customer-tour";
                                        });
                                    } else {
                                        showDialog("Thông báo", "Trả vé thất bại<br/> <b>" + rd + "</b>", null, function () {
                                        });
                                    }
                                });

                            });

                        }
                    },
                    {
                        label: 'Đóng',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }
                ]
            });

        });
    });
}

$(document).ready(function () {
    var id = GetParameterValues("id");
    console.log("id = " + id);
    //
    var params = {
        "tour_id": id
    };
    var url = "GetCustomerTour";
    tblExe.loadData(url, params, function (data) {
        var rows = data.rows;
        if (rows !== undefined && rows !== null && rows.length > 0) {
            hideAllModal();
            for (var i = 0; i < rows.length; ++i) {
                tblExe.addItem(rows[i]);
            }

            var cols = [
                {"data": "id"},
                {"data": "customer_name"},
                {"data": "phone"},
                {"data": "tour_name"},
                {"data": "status"},
                {"data": "start_time"},
                {"data": "register_time"},
                {
                    "data": null,
                    "className": "dt-center",
                    "orderable": false,
                    "render": function (data) {
                        var status = parseInt(data.status);
                        if (status === 0) {
                            return '<button class="btn btn-success">Trả vé</button>';
                        } else {
                            return "";
                        }

                    }
                }
            ];
            var defs = [
                {
                    "targets": 4,
                    "orderable": false,
                    "render": function (data) {
                        var sts = parseInt(data);
                        if (sts === 0) {
                            return "Đang chờ";
                        } else if (sts === 1) {
                            return "Đã hoàn thành";
                        } else if (sts === 0) {
                            return "Đã hủy";
                        }
                    }
                }

            ];
            tblExe.veBang($('#tbl_info'), cols, $("#tbl_container"), defs, rowCreatedCallBack);

        } else {
            hideAllModal();
        }
    });
});

