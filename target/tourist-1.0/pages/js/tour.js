

var tblExe = new TableExecute();

function rowCreatedCallBack(row, data, index) {
    $('td', row).eq(7).on('click', '.btn-success', function () {

    });
}
$(document).ready(function () {

    var params = {
    };
    var url = "GetListRealTourService";
    tblExe.loadData(url, params, function (data) {
        var rows = data.rows;
        if (rows !== undefined && rows !== null && rows.length > 0) {
            hideAllModal();
            for (var i = 0; i < rows.length; ++i) {
                tblExe.addItem(rows[i]);
            }
            var cols = [
                {"data": "id"},
                {"data": "name"},
                {"data": "deputation"},
                {"data": "num_of_customer"},
                {"data": "price"},
                {"data": "start_time"},
                {"data": "created_time"},
                {
                    "data": null,
                    "targets": 7,
                    "className": "dt-center",
                    "orderable": false,
                    "render": function (data) {
                        return '<a href="customer-tour?id=' + data.id + '" class="btn btn-success">Danh sách</a>';
                    }
                }
            ];
            var defs = [
                {
                    "targets": [3, 4],
                    "orderable": false,
                    "render": function (data) {
                        return data.formatMoneyEx(0);
                    }
                }

            ];
            tblExe.veBang($('#tbl_info'), cols, $("#tbl_container"), defs, rowCreatedCallBack);

        } else {
            hideAllModal();
        }
    });

});

