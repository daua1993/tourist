

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mt" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<mt:masterpage title="Danh sách đại lý">
    <jsp:attribute name="content">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Danh sách tour</h1>
            </div>
        </div>
        <div class="row" id="tbl_container">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tbl_info">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>Điểm bắt đầu</th>
                                <th>Điểm đến</th>
                                <th>Ngày tạo</th>
                                
                          
                            </tr>
                        </thead>
                        <tbody>

                        <tbody>
                    </table>
                </div>
            </div>
        </div>

    </jsp:attribute>

    <jsp:attribute name="jslink">
        <script src="js/utils.js" type="text/javascript"></script>
        <script src="js/defineds.js" type="text/javascript"></script>
        <link href="css/datepicker.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="js/tableexecute.js" type="text/javascript"></script>
        <script src="js/requestApi.js" type="text/javascript"></script>
        <script src="pages/js/list-tour.js" type="text/javascript"></script>
    </jsp:attribute>
</mt:masterpage>

