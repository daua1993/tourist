
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mt" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<mt:masterpage title="Danh sách khách hàng - Tour">
    <jsp:attribute name="content">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Danh sách khách hàng - Tour</h1>
            </div>
        </div>
        <div class="row" id="tbl_container">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tbl_info">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên Khách hàng</th>
                                <th>Số Điện thoại</th>
                                <th>Tên tour</th>
                                <th>Trạng thái</th>
                                <th>Thời gian khởi hành</th>
                                <th>Thời gian đăng ký</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        <tbody>
                    </table>
                </div>
            </div>
        </div>

    </jsp:attribute>

    <jsp:attribute name="jslink">
        <script src="js/utils.js" type="text/javascript"></script>
        <script src="js/defineds.js" type="text/javascript"></script>
        <link href="css/datepicker.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap-datepicker.js" type="text/javascript"></script>
        <script src="js/tableexecute.js" type="text/javascript"></script>
        <script src="js/requestApi.js" type="text/javascript"></script>
        <script src="pages/js/customer-tour.js" type="text/javascript"></script>
    </jsp:attribute>
</mt:masterpage>

