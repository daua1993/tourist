<%-- 
    Document   : line_chart
    Created on : Jul 18, 2016, 10:04:25 AM
    Author     : daua
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="row" id="chart_container" style="display: block">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <label id="txt_title_chart">Biều đồ</label>    
            </div>
            <div class="panel-body">
                <div class="flot-chart-content" id="line_chart"></div>
            </div>
        </div>
    </div>
</div>
