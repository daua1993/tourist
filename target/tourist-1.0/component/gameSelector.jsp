<%--
    Document   : gameSelector
    Created on : Jul 18, 2016, 8:41:21 AM
    Author     : daua
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<label>Game</label>
<select class="form-control" id="gameSelector" name="gameSelector">
<!--    <option value="100">100 - Ba cây</option>
    <option value="101">101 - Ba cây chương</option>
    <option value="102">102 - Phỏm</option>
    <option value="103">103 - Xì tố</option>
    <option value="104">104 - Tiến lên miền bắc</option>
    <option value="105">105 - Tiền lên miền nam</option>
    <option value="106">106 - Liêng</option>
    <option value="107">107 - Cờ tướng</option>
    <option value="108">108 - Tôm cua cá</option>
    <option value="109">109 - Chắn</option>
    <option value="110">110 - Cờ úp</option>
    <option value="111">111 - Sâm</option>
    <option value="113">113 - Tài xỉu</option>
    <option value="116">116 - Tiến lên đếm lá solo</option>
    <option value="131">131 - Tôm cua cá mini</option>
    <option value="132">132 - Cao thấp</option>
    <option value="134">134 - Đỏ đen</option>
    <option value="138">138 - Poker</option>
-->
    <option value="-1">Tất cả</option>
    <option value="112">112 - Tiến lên đếm lá</option>
    <option value="114">114 - Xóc đĩa</option>
    <option value="128">128 - Tài xỉu mini</option>
    <option value="130">130 - Poker mini</option>
    <option value="133">133 - Bầu tôm (new)</option>
    <option value="134">134 - VQMM</option>
    <option value="137">137 - Mậu binh</option>
    <option value="139">139 - Slot</option>
    <option value="140">140 - Catte</option>
    <option value="250">250 - Ba đôi</option>
</select>
