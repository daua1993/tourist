<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@attribute name="title" required="true" rtexprvalue="true"%>
<%@attribute name="content" fragment="true" %>
<%@attribute name="jslink" fragment="true" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <title>${title}</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="">
        <link href="<%=request.getContextPath()%>/css/loaders.min.css" rel="stylesheet" type="text/css"/>
        <link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<%=request.getContextPath()%>/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet" type="text/css"/>
        <link href="<%=request.getContextPath()%>/css/plugins/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%=request.getContextPath()%>/css/sb-admin-2.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link href="<%=request.getContextPath()%>/css/plugins/bootstrapValidator.css" rel="stylesheet" type="text/css"/>
        <link href="<%=request.getContextPath()%>/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Quản lý tour du lịch</a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="fa fa-user fa-fw"></i>Chào: Xuân Sơn Thân</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!--MENU VERTICAL ======================-->
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav metismenu" id="side-menu">
                            <li>
                                <a href="list-tour"> <i class="fas fa-tachometer-alt"></i> Danh mục tour</a>
                            </li>
                            <li>
                                <a href="tour"> <i class="fas fa-project-diagram"></i> Tour</a>
                            </li>
                            <li>
                                <a href="customer"> <i class="fas fa-dollar-sign"></i> Khách hàng</a>
                            </li>
                            <li>
                                <a href="customer-tour"> <i class="fas fa-chart-bar"></i> Khách hàng - Tour</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div id="page-wrapper">
                <jsp:invoke fragment="content"/>
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="mdLoading" data-backdrop="static">
                <div class="modal-dialog modal-sm">
                    <div data-loader="circle" style="margin: 1em auto"></div>
                </div>
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="confirmDialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title_confirm">Thông báo</h4>
                        </div>
                        <div class="modal-body">
                            <p id="content_confirm">Vui lòng nhập đủ dữ liệu !</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                            <button type="button" class="btn btn-primary" id="confirm_dilog_btn_ok">Ban</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="globalModal">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title_alert">Thông báo</h4>
                        </div>
                        <div class="modal-body">
                            <p id="content_alert">Vui lòng nhập đủ dữ liệu !</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="<%=request.getContextPath()%>/js/mlogger.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/jquery.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/plugins/metisMenu/metisMenu.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/plugins/dataTables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/plugins/dataTables/dataTables.bootstrap.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/sb-admin-2.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/bootstrap-dialog.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/plugins/dataTables/sorttime1.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/plugins/bootstrapValidator.js" type="text/javascript"></script>
        <jsp:invoke fragment="jslink"/>
    </body>
</html>

