<%-- 
    Document   : select_tran_type
    Created on : Jul 19, 2016, 9:08:54 AM
    Author     : daua
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<select class="form-control" id="select_tran_type">
    <option value="-1">--Chọn kiểu giao dịch--</option>
    <option value="1">Parner_Gold mua gold từ bank</option>
    <option value="2">Parner_Gold bán gold cho bank</option>
    <option value="3">Parner_Gold bán gold cho người chơi</option>
    <option value="4">Parner_Gold mua lại gold của người chơi</option>
    <option value="5">Partner_Gold trao đổi gold với nhau</option>
    <option value="6">Người chơi nạp gold</option>
    <option value="7">Người chơi chuyển tiền cho người chơi khác</option>
    <option value="8">Người chơi chơi game trong hệ thống bị trừ hoặc cộng tiền</option>
    <option value="9">Người chơi được tặng gold</option>
    <option value="10">Admin nạp tiền vào bank</option>
    <option value="11">Cộng tiền cho người chơi khi hoàn thành nhiệm vụ đăng ký facebook</option>
    <option value="12">Cộng tiền cho người chơi khi liên kết Facebook</option>
    <option value="13">Cộng tiền cho người chơi khi điểm danh</option>
    <option value="14">Đổi thưởng</option>
    <option value="15">Giao dịch quay thưởng</option>
    <option value="16">Giao dịch nạp tiền InApp</option>
</select>
