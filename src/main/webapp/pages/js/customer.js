/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tblExe = new TableExecute();

$(document).ready(function () {

    var params = {
    };
    var url = "GetListCustomerService";
    tblExe.loadData(url, params, function (data) {
        var rows = data.rows;
        if (rows !== undefined && rows !== null && rows.length > 0) {
            hideAllModal();
            for (var i = 0; i < rows.length; ++i) {
                tblExe.addItem(rows[i]);
            }
            var cols = [
                {"data": "id"},
                {"data": "name"},
                {"data": "idNumber"},
                {"data": "idType"},
                {"data": "phone"},
                {"data": "email"},
                {"data": "address"},
                {"data": "createdTime"}
            ];

            var defs = [

            ];
            tblExe.veBang($('#tbl_info'), cols, $("#tbl_container"), defs);

        } else {
            hideAllModal();
        }
    });

});

