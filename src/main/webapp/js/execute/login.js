/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global BootstrapDialog */

$(document).ready(function () {

    var options = {
        message: 'This value is not valid',
        live: 'enabled',
        framework: 'bootstrap',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: "Vui lòng nhập username"
                    },
                    stringLength: {
                        min: 4,
                        max: 30,
                        message: 'Độ dài từ 4 - 30 ký tự'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'Tên đăng nhập không đúng định dạng'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: "Vui lòng nhập mật khẩu"
                    },
                    stringLength: {
                        min: 6,
                        message: "Mật khẩu >= 6 ký tự"
                    }
                }
            }
        }
    };
    $('#loginform').bootstrapValidator(options)
            .on('success.form.bv', function (e) {
                e.preventDefault();
                $("#send_btn").attr("disabled", false);
                var data = $(e.target).serializeArray();
                login(data);
            });
});

function getValue(array, key) {
    for (var i = 0; i < array.length; ++i) {
        if (array[i].name === key) {
            return array[i].value;
        }
    }
    return "";
}

function login(data) {

    console.log("data: " + JSON.stringify(data));
    var username = getValue(data, "username");
    var password = getValue(data, "password");

    var req = new Request();
    $("#btnLogin").attr("disabled", false);
    var params = {
        username: username,
        password: password
    };
    req.request("LoginService", params, function (data) {
        if (data !== undefined && data !== null) {
            var rc = data.rc;
            var rd = data.rd;
            if (rc === 0) {
                localStorage['username'] = username;
                localStorage['role'] = data.role;
                console.log("role: " + data.role + " typeOf: " + typeof (data.role));
                var first_page = data.first_page;
                if (first_page === undefined || first_page === null || first_page.length === 0) {
                    first_page = "TongQuanAdmin";
                }
                window.location.replace(first_page);
            } else {
                showDialog("Thông báo", "Đăng nhập thất bại <br/>" + rd, null, function (res) {
                    $("#username").focus();
                });
            }
        } else {
            hideAllModal();
        }
    });
}