
var Request = function () {
};

/*
 *
 * @param {String} url
 * @param {JsonObject} params
 * @param {function} fnCallBack
 * @param {"Poset" "Get"} method
 * @returns {void}
 */
Request.prototype.request = function (url, params, fnCallBack, method) {
    var s = "";
    var k = 0;
    for (var i in params) {
        if (k > 0) {
            s += "&";
        }
        s += i;
        s += "=";
        s += params[i];
        k++;
    }
    s = s.trim();
    console.log("link: " + url + "?" + s);

    if (method === undefined) {
        method = 'GET';
    }

    showLoading(true);
    $.ajax({
        type: method,
        url: url,
        data: params,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
//            console.log("received= " + $.parseJSON(data.Parameters));
            var jsonData = JSON.stringify(data);
            console.log("success: " + jsonData + " textStatus: " + textStatus + " jqXHR: " + jqXHR.status);
            fnCallBack(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var status = jqXHR.status;
            var s;
            if (status === 0) {
                s = "<b>Không có kết nối mạng !</b>";
            } else if (errorThrown !== null && errorThrown !== undefined && typeof errorThrown === 'string' && errorThrown.toLowerCase() === "not found") {
                s = "<b>Không tìm thấy yêu cầu mà bạn muốn thực hiện</b>";
            } else {
                s = jqXHR.responseText;
            }
            console.log("error: jqXHR: " + JSON.stringify(jqXHR) + " textStatus: " + textStatus + " errorThrown: " + errorThrown);
            showDialog("<b>Thông báo</b>", s);
        }
    });
};

Request.prototype.requestForward = function (params, fnCallBack, onFailed) {
    var url = "ForwardRequest";
    var method = 'GET';
    var s = "";
    for (var i in params) {
        s += i;
        s += "=";
        s += params[i];
        s += "&";
    }
    s.trim();
    if (s.length > 0)
        s = s.substring(0, s.length - 1);
    console.log("link: " + url + "?" + s);

    showLoading(true);
    $.ajax({
        type: method,
        url: url,
        data: params,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
//            console.log("received= " + $.parseJSON(data.Parameters));
            var jsonData = JSON.stringify(data);
            console.log("success: " + jsonData + " textStatus: " + textStatus + " jqXHR: " + jqXHR.status);
            fnCallBack(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var status = jqXHR.status;
            var s;
            if (status === 0) {
                s = "Không có kết nối mạng !";
            } else {
                s = jqXHR.responseText;
            }
            console.log("error: jqXHR: " + JSON.stringify(jqXHR) + " textStatus: " + textStatus + " errorThrown: " + errorThrown);
            showDialog("Lỗi", s);
            if (onFailed !== undefined) {
                onFailed();
            }
        }
    });
};

Request.prototype.requestForwardNoModal = function (params, fnCallBack, onFailed) {
    var url = "ForwardRequest";
    var method = 'GET';
    var s = "";
    for (var i in params) {
        s += i;
        s += "=";
        s += params[i];
        s += "&";
    }
    s.trim();
    if (s.length > 0)
        s = s.substring(0, s.length - 1);
    console.log("link: " + url + "?" + s);

    $.ajax({
        type: method,
        url: url,
        data: params,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            var jsonData = JSON.stringify(data);
            console.log("success: " + jsonData + " textStatus: " + textStatus + " jqXHR: " + jqXHR.status);
            fnCallBack(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var status = jqXHR.status;
            var s;
            if (status === 0) {
                s = "Không có kết nối mạng !";
            } else {
                s = jqXHR.responseText;
            }
            console.log("error: jqXHR: " + JSON.stringify(jqXHR) + " textStatus: " + textStatus + " errorThrown: " + errorThrown);
            showDialog("Lỗi", s);
            if (onFailed !== undefined) {
                onFailed();
            }
        }
    });
};

Request.prototype.requestNoModal = function (url, params, fnCallBack, method, onFailed) {
    var s = "";
    for (var i in params) {
        s += i;
        s += "=";
        s += params[i];
        s += "&";
    }
    s.trim();
    if (s.length > 0)
        s = s.substring(0, s.length - 1);
    console.log("link: " + url + "?" + s);
    if (method === undefined) {
        method = 'GET';
    }
    $.ajax({
        type: method,
        url: url,
        data: params,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            var jsonData = JSON.stringify(data);
            console.log("success: " + jsonData + " textStatus: " + textStatus + " jqXHR: " + jqXHR.status);
            fnCallBack(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var status = jqXHR.status;
            var s;
            if (status === 0) {
                s = "Không có kết nối mạng !";
            } else {
                s = jqXHR.responseText;
            }
            console.log("error: jqXHR: " + JSON.stringify(jqXHR) + " textStatus: " + textStatus + " errorThrown: " + errorThrown);
            if (onFailed !== undefined) {
                onFailed();
            }
        }
    });
};

/**
 *
 * @param {type} url : url request
 * @param {type} params : params request
 * @param {type} fnSuccess : function callback if Success
 * @param {type} fnError : function callback if error.
 * @returns {undefined}
 */
Request.prototype.postRequest = function (url, params, fnSuccess, fnError) {
    var s = "";
    for (var i in params) {
        s += i;
        s += "=";
        s += params[i];
        s += "&";
    }
    s.trim();
    if (s.length > 0)
        s = s.substring(0, s.length - 1);
    console.log("link: " + url + "?" + s);

    showLoading(true);
    $.ajax({
        type: "POST",
        url: url,
        data: params,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            var jsonData = JSON.stringify(data);
            console.log("success: " + jsonData + " textStatus: " + textStatus + " jqXHR: " + jqXHR.status);
            fnSuccess(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            hideAllModal();
            var status = jqXHR.status;
            var s;
            if (status === 0) {
                s = "Không có kết nối mạng !";
            } else {
                s = jqXHR.responseText;
            }
            console.log("error: jqXHR: " + JSON.stringify(jqXHR) + " textStatus: " + textStatus + " errorThrown: " + errorThrown);
            fnError(jqXHR);
            showDialog("Lỗi", s);
        }
    });
};

Request.prototype.postRequestNoLoading = function (url, params, fnSuccess, fnError) {
    var s = "";
    for (var i in params) {
        s += i;
        s += "=";
        s += params[i];
        s += "&";
    }
    s.trim();
    if (s.length > 0)
        s = s.substring(0, s.length - 1);
    console.log("link: " + url + "?" + s);

    $.ajax({
        type: "POST",
        url: url,
        data: params,
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            var jsonData = JSON.stringify(data);
            console.log("success: " + jsonData + " textStatus: " + textStatus + " jqXHR: " + jqXHR.status);
            fnSuccess(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            hideAllModal();
            var status = jqXHR.status;
            var s;
            if (status === 0) {
                s = "Không có kết nối mạng !";
            } else {
                s = jqXHR.responseText;
            }
            console.log("error: jqXHR: " + JSON.stringify(jqXHR) + " textStatus: " + textStatus + " errorThrown: " + errorThrown);
            fnError(jqXHR);
            showDialog("Lỗi", s);
        }
    });
};

Request.prototype.postJsonRequest = function (url, params, fnSuccess, fnError) {

    console.log("link: " + url + " : " + JSON.stringify(params));

    showLoading(true);
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(params),
        dataType: 'json',
        contentType: 'application/json',
        success: function (data, textStatus, jqXHR) {
            var jsonData = JSON.stringify(data);
            console.log("success: " + jsonData + " textStatus: " + textStatus + " jqXHR: " + jqXHR.status);
            fnSuccess(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            hideAllModal();
            var status = jqXHR.status;
            var s;
            if (status === 0) {
                s = "Không có kết nối mạng !";
            } else {
                s = jqXHR.responseText;
            }
            console.log("error: jqXHR: " + JSON.stringify(jqXHR) + " textStatus: " + textStatus + " errorThrown: " + errorThrown);
            fnError(jqXHR);
            showDialog("Lỗi", s);
        }
    });
};
