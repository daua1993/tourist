/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var __threeDayBefore = new Date().getDate() - 7;
var __date = new Date();
__date.setDate(__threeDayBefore);
var __nextday = new Date().getDate();
var __date1 = new Date();
__date1.setDate(__nextday);
!function (a) {
    a.fn.datepicker.dates.vi = {
        days: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
        daysShort: ["CN", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"],
        daysMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        months: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
        monthsShort: ["Th1", "Th2", "Th3", "Th4", "Th5", "Th6", "Th7", "Th8", "Th9", "Th10", "Th11", "Th12"],
        today: "Hôm nay",
        clear: "Xóa",
        format: "dd/mm/yyyy"
    };
}(jQuery);
$('#dateFrom').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayBtn: "linked",
    todayHighlight: true,
    language: "vi"
}).datepicker("setDate", __date);
$('#dateTo').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayBtn: "linked",
    todayHighlight: true,
    language: "vi"
}).datepicker("setDate", __date1);
var TableExecute = function () {
    this.array = [];
    this.total = 0;
    this.positionPage = 0;
    this.paggin = true;
    this.order = [];
    this.searching = true;
};
TableExecute.prototype.setTotal = function (total) {
    this.total = total;
};
TableExecute.prototype.pagging = function (_page) {
    this.paggin = _page;
};
TableExecute.prototype.setOrder = function (_order) {
    this.order = _order;
};
TableExecute.prototype.setSearching = function (_searching) {
    this.searching = _searching;
};
TableExecute.prototype.reset = function () {
    this.array.splice(0, this.array.length);
    this.total = 0;
    this.positionPage = 0;
};
TableExecute.prototype.getPositionPage = function () {
    return this.positionPage;
};
TableExecute.prototype.addItem = function (item) {
    this.array.push(item);
    this.positionPage++;
};
TableExecute.prototype.setDataSet = function (ds) {
    this.array = [];
    this.array = ds;
};
/**
 *
 * @param {String} url
 * @param {Object} params
 * @param {function} fnCallBack
 * @returns {undefined}
 */
TableExecute.prototype.loadData = function (url, params, fnCallBack) {

    if (this.total > 0 && this.positionPage > 0 && this.positionPage >= this.total) {
        console.log("pos = " + this.positionPage + " total = " + this.total + " can't load more");
        return;
    }

    var s = "";
    for (var i in params) {
        s += i;
        s += "=";
        s += params[i];
        s += "&";
    }
    s.trim();
    if (s.length > 0)
        s = s.substring(0, s.length - 1);
    console.log("link: " + url + "?" + s);
    showLoading(true);
    $.ajax({
        type: 'GET',
        url: url,
        data: params,
        dataType: 'json',
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            var jsonData = JSON.stringify(data);
            console.log("success: " + jsonData + " textStatus: " + textStatus + " jqXHR: " + jqXHR.status);
            fnCallBack(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var status = jqXHR.status;
            var s;
            if (status === 0) {
                s = "Không có kết nối mạng !";
            } else {
                if (status === 200 && jqXHR.responseText === "") {
                    s = "Không có dữ liệu";
                } else {
                    s = jqXHR.responseText;
                }
            }
            console.log("error: jqXHR: " + JSON.stringify(jqXHR) + " textStatus: " + textStatus + " errorThrown: " + errorThrown);
            showDialog("Lỗi", s);
        }
    });
};

/**
 *
 * @param {String} url
 * @param {Object} params
 * @param {function} fnCallBack
 * @returns {undefined}
 */
TableExecute.prototype.loadDataNoLoading = function (url, params, fnCallBack) {

    if (this.total > 0 && this.positionPage > 0 && this.positionPage >= this.total) {
        console.log("pos = " + this.positionPage + " total = " + this.total + " can't load more");
        return;
    }

    var s = "";
    for (var i in params) {
        s += i;
        s += "=";
        s += params[i];
        s += "&";
    }
    s.trim();
    if (s.length > 0)
        s = s.substring(0, s.length - 1);
    console.log("link: " + url + "?" + s);
    $.ajax({
        type: 'GET',
        url: url,
        data: params,
        dataType: 'json',
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            var jsonData = JSON.stringify(data);
            console.log("success: " + jsonData + " textStatus: " + textStatus + " jqXHR: " + jqXHR.status);
            fnCallBack(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var status = jqXHR.status;
            var s;
            if (status === 0) {
                s = "Không có kết nối mạng !";
            } else {
                if (status === 200 && jqXHR.responseText === "") {
                    s = "Không có dữ liệu";
                } else {
                    s = jqXHR.responseText;
                }
            }
            console.log("error: jqXHR: " + JSON.stringify(jqXHR) + " textStatus: " + textStatus + " errorThrown: " + errorThrown);
            showDialog("Lỗi", s);
        }
    });
};
/**
 *
 * @param {type} index
 * @param {type} keys
 * @param {type} vals
 * @returns {undefined}
 */
TableExecute.prototype.edit = function (index, keys, vals) {
    var obj = this.array[index];
    for (var i = 0; i < keys.length; ++i) {
        obj[keys[i]] = vals[i];
    }
};
/**
 *
 * @param {Integer} index
 * @returns {undefined}
 */
TableExecute.prototype.remove = function (index) {
    this.array.splice(index, 1);
};
TableExecute.prototype.dataSet = function () {
    return this.array;
};
/**
 *
 * @param {<table></table>} tbl : Bảng được lấy từ jquery $("$id_table");
 * @param {Array} colums : Các cột được hiển thị (phải lấy trong this.array)
 * @param {<div></div>} divParent : Thẻ div chứa table.
 * @param {Array} columnDefs columsDefs
 * @param {fnRowCreated} fnRowCreated Hàm call back sau mỗi dòng created
 * @param {bAutoWidth} bAutoWidth auto width hay không
 * @returns {undefined}
 */

TableExecute.prototype.veBang = function (options) {
    var tbl = options.tbl;
    var colums = options.colums;
    var divParent = options.divParent;
    var columnDefs = options.columnDefs;
    var fnRowCreated = options.fnRowCreated;
    var bAutoWidth = options.bAutoWidth;
    var alwayShowMore = options.alwayShowMore;
    var btnMore = options.btnMore;

    console.log("start draw table");
    if (columnDefs === undefined || columnDefs === null) {
        columnDefs = [];
    }
    if (bAutoWidth === undefined || bAutoWidth === null) {
        bAutoWidth = true;
    }
    if (alwayShowMore === undefined || alwayShowMore === null) {
        alwayShowMore = false;
    }
    var t = tbl.DataTable({
        "oSearch": {"sSearch": ""},
        "data": this.array,
        "columns": colums,
        "autoWidth": bAutoWidth,
        "destroy": true,
        "columnDefs": columnDefs,
        "stateSave": true,
        "order": this.order,
        "searching": this.searching,
        "paging": this.paggin,
        "pageLength": 100,
        "fnDrawCallback": function (settings) {
            if (divParent !== undefined && divParent !== null) {
                divParent.toggle(true);
            }
        },
        "createdRow": function (row, data, index) {
            if (fnRowCreated !== undefined) {
                fnRowCreated(row, data, index);
            }
        }
    });
    t.search('').draw();
    if (!alwayShowMore) {
        if (btnMore !== undefined && btnMore !== null) {
            if (this.total === 0 && this.positionPage === 0) {
                btnMore.toggle(false);
            } else if (this.total === 0 && this.positionPage > 0) {
                btnMore.toggle(false);
            } else if (this.total > 0 && this.positionPage >= this.total) {
                btnMore.toggle(false);
            } else {
                btnMore.toggle(true);
            }
        }
    }
    return t;
};

TableExecute.prototype.veBang = function (tbl, colums, divParent, columnDefs, fnRowCreated, bAutoWidth, alwayShowMore) {
    console.log("start draw table");
    if (columnDefs === undefined || columnDefs === null) {
        columnDefs = [];
    }
    if (bAutoWidth === undefined || bAutoWidth === null) {
        bAutoWidth = true;
    }
    if (alwayShowMore === undefined || alwayShowMore === null) {
        alwayShowMore = false;
    }
    var t = tbl.DataTable({
        "oSearch": {"sSearch": ""},
        "data": this.array,
        "columns": colums,
        "autoWidth": bAutoWidth,
        "destroy": true,
        "columnDefs": columnDefs,
        "stateSave": true,
        "order": this.order,
        "searching": this.searching,
        "paging": this.paggin,
        "fnDrawCallback": function (settings) {
            if (divParent !== undefined && divParent !== null) {
                divParent.toggle(true);
            }
        },
        "createdRow": function (row, data, index) {
            if (fnRowCreated !== undefined) {
                fnRowCreated(row, data, index);
            }
        }
    });
    t.search('').draw();
    if (!alwayShowMore) {
        var btnMore = $("#btnMore");
        if (btnMore !== undefined && btnMore !== null) {
            if (this.total === 0 && this.positionPage === 0) {
                btnMore.toggle(false);
            } else if (this.total === 0 && this.positionPage > 0) {
                btnMore.toggle(false);
            } else if (this.total > 0 && this.positionPage >= this.total) {
                btnMore.toggle(false);
            } else {
                btnMore.toggle(true);
            }
        }
    }
    return t;
};

TableExecute.prototype.veBangReport = function (tbl, reportData, divParent, columnDefs, fnRowCreated, bAutoWidth, alwayShowMore) {
    console.log("start draw table");

    var i;
    var columnHeaders = reportData.columnHeaders;
    var columns = [];
    for (i = 0; i < columnHeaders.length; i++) {
        var column = {title: columnHeaders[i]};
        columns.push(column);
    }

    if (columnDefs === undefined || columnDefs === null) {
        columnDefs = [];
    }
    if (bAutoWidth === undefined || bAutoWidth === null) {
        bAutoWidth = true;
    }
    if (alwayShowMore === undefined || alwayShowMore === null) {
        alwayShowMore = false;
    }
    var t = tbl.DataTable({
        "oSearch": {"sSearch": ""},
        "data": reportData.data,
        "columns": columns,
        "autoWidth": bAutoWidth,
        "destroy": true,
        "columnDefs": columnDefs,
        "stateSave": true,
        "order": this.order,
        "searching": this.searching,
        "paging": this.paggin,
        "fnDrawCallback": function (settings) {
            if (divParent !== undefined && divParent !== null) {
                divParent.toggle(true);
            }
        },
        "createdRow": function (row, data, index) {
            if (fnRowCreated !== undefined) {
                fnRowCreated(row, data, index);
            }
        }
    });
    t.search('').draw();
    if (!alwayShowMore) {
        var btnMore = $("#btnMore");
        if (btnMore !== undefined && btnMore !== null) {
            if (this.total === 0 && this.positionPage === 0) {
                btnMore.toggle(false);
            } else if (this.total === 0 && this.positionPage > 0) {
                btnMore.toggle(false);
            } else if (this.total > 0 && this.positionPage >= this.total) {
                btnMore.toggle(false);
            } else {
                btnMore.toggle(true);
            }
        }
    }
    return t;
};


TableExecute.prototype.veBangWithRowGroup = function (tbl, divParent, tableOptions, fnRowCreated, bAutoWidth, alwayShowMore) {
    console.log("start draw table");
    if (bAutoWidth === undefined || bAutoWidth === null) {
        bAutoWidth = true;
    }
    if (alwayShowMore === undefined || alwayShowMore === null) {
        alwayShowMore = false;
    }
    var t = tbl.DataTable({
        "oSearch": {"sSearch": ""},
        "data": this.array,
        "columns": tableOptions.columns,
        "autoWidth": bAutoWidth,
        "destroy": true,
        "columnDefs": (tableOptions.columnDefs === undefined || tableOptions.columnDefs === null) ? [] : tableOptions.columnDefs,
        "stateSave": true,
        "order": this.order,
        "searching": this.searching,
        "orderFixed": (tableOptions.orderFixed === undefined || tableOptions.orderFixed === null) ? [] : tableOptions.orderFixed,
        "paging": false,
        "fnDrawCallback": function (settings) {
            if (divParent !== undefined && divParent !== null) {
                divParent.toggle(true);
            }
        },
        "createdRow": function (row, data, index) {
            if (fnRowCreated !== undefined) {
                fnRowCreated(row, data, index);
            }
        },
        "rowGroup": tableOptions.rowGroup,
        "pageLength": (tableOptions.pageLength === undefined || tableOptions.pageLength === null) ? 100 : tableOptions.pageLength
    });
    t.search('').draw();
    if (!alwayShowMore) {
        var btnMore = $("#btnMore");
        if (btnMore !== undefined && btnMore !== null) {
            if (this.total === 0 && this.positionPage === 0) {
                btnMore.toggle(false);
            } else if (this.total === 0 && this.positionPage > 0) {
                btnMore.toggle(false);
            } else if (this.total > 0 && this.positionPage >= this.total) {
                btnMore.toggle(false);
            } else {
                btnMore.toggle(true);
            }
        }
    }
    return t;
};
TableExecute.prototype.veBangEx = function (tbl, colums, columnDefs, fnRowCreated, bAutoWidth, alwayShowMore) {
    console.log("start draw table");
    if (columnDefs === undefined || columnDefs === null) {
        columnDefs = [];
    }
    if (bAutoWidth === undefined || bAutoWidth === null) {
        bAutoWidth = true;
    }
    if (alwayShowMore === undefined || alwayShowMore === null) {
        alwayShowMore = false;
    }
    var t = tbl.DataTable({
        "data": this.array,
        "columns": colums,
        "autoWidth": bAutoWidth,
        "destroy": true,
        "columnDefs": columnDefs,
        "stateSave": true,
        "order": this.order,
        "searching": this.searching,
        "lengthMenu": [[10], [10]],
        "info": false,
        "bFilter": false,
        "paging": true,
        "bLengthChange": false,
        "oSearch": {"sSearch": ""},
        "dom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
        "createdRow": function (row, data, index) {
            if (fnRowCreated !== undefined) {
                fnRowCreated(row, data, index);
            }
        }
    });
    t.search('').draw();
    if (!alwayShowMore) {
//        var btnMore = $("#btnMore");
//        if (btnMore !== undefined && btnMore !== null) {
//            if (this.total === 0 && this.positionPage === 0) {
//                btnMore.toggle(false);
//            } else if (this.total === 0 && this.positionPage > 0) {
//                btnMore.toggle(false);
//            } else if (this.total > 0 && this.positionPage >= this.total) {
//                btnMore.toggle(false);
//            } else {
//                btnMore.toggle(true);
//            }
//        }
    }
    return t;
};
TableExecute.prototype.veBangExcel = function (titleFile, tbl, colums, divParent, columnDefs, fnRowCreated, bAutoWidth) {
    console.log("start draw table excel");
    if (columnDefs === undefined || columnDefs === null) {
        columnDefs = [];
    }
    if (bAutoWidth === undefined || bAutoWidth === null) {
        bAutoWidth = true;
    }
    var t = tbl.DataTable({
        "dom": 'Bfrtip',
        "buttons": [
            {
                extend: 'excelHtml5',
                text: 'Xuất Excel',
                title: titleFile
            }

        ],
        "data": this.array,
        "columns": colums,
        "autoWidth": bAutoWidth,
        "destroy": true,
        "columnDefs": columnDefs,
        "stateSave": true,
        "fnDrawCallback": function (settings) {
            if (divParent !== undefined && divParent !== null) {
                divParent.toggle(true);
            }
        },
        "createdRow": function (row, data, index) {
            if (fnRowCreated !== undefined) {
                fnRowCreated(row, data, index);
            }
        }
    });
    t.search('').draw();
    var btnMore = $("#btnMore");
    if (btnMore !== undefined && btnMore !== null) {
        if (this.total === 0 && this.positionPage === 0) {
            btnMore.toggle(false);
        } else if (this.total === 0 && this.positionPage > 0) {
            btnMore.toggle(false);
        } else if (this.total > 0 && this.positionPage >= this.total) {
            btnMore.toggle(false);
        } else {
            btnMore.toggle(true);
        }
    }
    return t;
};
