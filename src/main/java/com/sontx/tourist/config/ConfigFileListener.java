package com.sontx.tourist.config;

import org.jconfig.event.FileListener;
import org.jconfig.event.FileListenerEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigFileListener implements FileListener {

    private static ConfigFileListener instance;
    private final String configName;
    private final Logger logger = LoggerFactory.getLogger(ConfigFileListener.class);

    public synchronized static ConfigFileListener getInstance(String configName) {
        if (instance == null) {
            instance = new ConfigFileListener(configName);
        }
        return instance;
    }

    private ConfigFileListener(String configName) {
        this.configName = configName;
    }


    @Override
    public void fileChanged(FileListenerEvent arg0) {
        try {
            AppConfig.reload();
            logger.info("Config file changed. System config reloaded and posting event to all subscribers");
        } catch (Exception ex) {
            logger.error("", ex);
        }
    }

}
