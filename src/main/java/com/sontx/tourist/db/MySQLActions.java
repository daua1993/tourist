package com.sontx.tourist.db;

import com.google.common.base.Strings;
import com.kiemanh.vn.DbConnection;
import com.sontx.tourist.common.AppUtils;
import com.sontx.tourist.entity.Customer;
import com.sontx.tourist.entity.CustomerTour;
import com.sontx.tourist.entity.CustomerTourDetail;
import com.sontx.tourist.entity.RealTour;
import com.sontx.tourist.entity.Tour;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySQLActions {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public long countPlayHistory(String uid, String gameId, String subGameId) {
        long count = 0;
        String sql = "SELECT count(*) c FROM tbl_logs_machine where user_id = ? ";

        if (!Strings.isNullOrEmpty(gameId)) {
            sql += " and game_id = ? ";
        }

        if (!Strings.isNullOrEmpty(subGameId)) {
            sql += " and sub_game_id = ? ";
        }

        DbConnection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = MySQLAccess.getInstance().getConn();
            ps = connection.prepareStatement(sql);
            int index = 1;
            ps.setString(index++, uid);
            if (!Strings.isNullOrEmpty(gameId)) {
                ps.setString(index++, gameId);
            }
            if (!Strings.isNullOrEmpty(subGameId)) {
                ps.setString(index, subGameId);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getLong("c");
            }
        } catch (SQLException ex) {
            logger.error("DB error =>", ex);
        } finally {
            MySQLAccess.getInstance().closeResultSet(rs);
            MySQLAccess.getInstance().closePreparedStatement(ps);
            MySQLAccess.getInstance().closeConn(connection);
        }
        return count;
    }

    public List<Customer> getCustomer() {
        List<Customer> result = new ArrayList<>();
        String sql = "SELECT * FROM tbl_customer order by created_time desc ";
        DbConnection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = MySQLAccess.getInstance().getConn();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Customer tour = new Customer();
                tour.setId(AppUtils.parseInt(rs.getObject("id")));
                tour.setName(AppUtils.parseString(rs.getObject("name")));
                tour.setIdNumber(AppUtils.parseInt(rs.getObject("id_number")));
                tour.setIdType(AppUtils.parseInt(rs.getObject("id_type")));
                tour.setPhone(rs.getString("phone"));
                tour.setEmail(rs.getString("email"));
                tour.setAddress(rs.getString("address"));
                tour.setCreatedTime(rs.getTimestamp("created_time"));
                result.add(tour);
            }
        } catch (SQLException ex) {
            logger.error("DB error =>", ex);
        } finally {
            MySQLAccess.getInstance().closeResultSet(rs);
            MySQLAccess.getInstance().closePreparedStatement(ps);
            MySQLAccess.getInstance().closeConn(connection);
        }
        return result;
    }

    public List<Tour> getListTour() {
        List<Tour> result = new ArrayList<>();
        String sql = "SELECT * FROM tbl_tour order by created_time desc ";
        DbConnection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = MySQLAccess.getInstance().getConn();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Tour tour = new Tour();
                tour.setId(AppUtils.parseInt(rs.getObject("id")));
                tour.setName(AppUtils.parseString(rs.getObject("name")));
                tour.setStartLocation(AppUtils.parseString(rs.getObject("location_start")));
                tour.setEndLocation(AppUtils.parseString(rs.getObject("location_destination")));
                tour.setUpdatedTime(rs.getTimestamp("updated_time"));
                result.add(tour);
            }
        } catch (SQLException ex) {
            logger.error("DB error =>", ex);
        } finally {
            MySQLAccess.getInstance().closeResultSet(rs);
            MySQLAccess.getInstance().closePreparedStatement(ps);
            MySQLAccess.getInstance().closeConn(connection);
        }
        return result;
    }

    public List<RealTour> getRealTour() {
        List<RealTour> result = new ArrayList<>();
        String sql = "select a.*, b.name from tbl_real_tour a inner join tbl_tour b on a.tour_id = b.id ";
        sql += " order by start_time asc ";
        DbConnection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = MySQLAccess.getInstance().getConn();
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                RealTour tour = new RealTour();
                tour.setId(AppUtils.parseInt(rs.getObject("id")));
                tour.setName(AppUtils.parseString(rs.getObject("name")));
                tour.setTourId(AppUtils.parseInt(rs.getObject("tour_id")));
                tour.setDeputation(AppUtils.parseString(rs.getObject("deputation")));
                tour.setPrice(AppUtils.parseInt(rs.getObject("price")));
                tour.setPaid(AppUtils.parseInt(rs.getObject("paid")));
                tour.setNumOfCustomer(AppUtils.parseInt(rs.getObject("num_of_customer")));
                tour.setDescription(AppUtils.parseString(rs.getObject("description")));
                tour.setStartTime(rs.getTimestamp("start_time"));
                tour.setCreatedTime(rs.getTimestamp("created_time"));
                tour.setUpdatedTime(rs.getTimestamp("updated_time"));
                result.add(tour);
            }
        } catch (SQLException ex) {
            logger.error("DB error =>", ex);
        } finally {
            MySQLAccess.getInstance().closeResultSet(rs);
            MySQLAccess.getInstance().closePreparedStatement(ps);
            MySQLAccess.getInstance().closeConn(connection);
        }
        return result;
    }

    public int refunTicket(int ticketId) {
        return -1;
    }

    public CustomerTourDetail findOneCustomerTour(int ticketId) {
        CustomerTourDetail tour = null;
        String sql = " select a.*, b.name as customer_name, b.phone,  "
                + "c.price, c.deputation, c.num_of_customer, c.start_time, "
                + "d.name as tour_name, d.location_start, d.location_destination "
                + "from tbl_customer_tour a  "
                + "inner join tbl_customer b on a.customer_id = b.id "
                + "inner join tbl_real_tour c on a.tour_id = c.id "
                + "inner join tbl_tour d on c.tour_id = d.id "
                + "where a.id = ? limit 1";
        DbConnection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = MySQLAccess.getInstance().getConn();
            ps = connection.prepareStatement(sql);
            ps.setInt(1, ticketId);
            rs = ps.executeQuery();
            if (rs.next()) {
                tour = new CustomerTourDetail();
                tour.setId(AppUtils.parseInt(rs.getObject("id")));
                tour.setCustomer_id(AppUtils.parseInt(rs.getObject("customer_id")));
                tour.setCustomer_name(AppUtils.parseString(rs.getObject("customer_name")));
                tour.setPhone(AppUtils.parseString(rs.getObject("phone")));
                tour.setTour_id(AppUtils.parseInt(rs.getObject("tour_id")));
                tour.setTour_name(AppUtils.parseString(rs.getObject("tour_name")));
                tour.setStatus(AppUtils.parseInt(rs.getObject("status")));
                tour.setRegister_time(rs.getTimestamp("register_time"));
                tour.setStart_time(rs.getTimestamp("start_time"));
                tour.setCreated_time(rs.getTimestamp("created_time"));
                tour.setUpdated_time(rs.getTimestamp("updated_time"));
                tour.setLocationStart(AppUtils.parseString(rs.getObject("location_start")));
                tour.setLocationEnd(AppUtils.parseString(rs.getObject("location_destination")));
                tour.setDeputation(AppUtils.parseString(rs.getObject("deputation")));
                tour.setPrice(AppUtils.parseInt(rs.getObject("price")));
                tour.setNumberOfGuess(AppUtils.parseInt(rs.getObject("num_of_customer")));
            }
        } catch (SQLException ex) {
            logger.error("DB error =>", ex);
        } finally {
            MySQLAccess.getInstance().closeResultSet(rs);
            MySQLAccess.getInstance().closePreparedStatement(ps);
            MySQLAccess.getInstance().closeConn(connection);
        }
        return tour;
    }

    //
    public List<CustomerTour> getCustomerTour(int tourId) {
        List<CustomerTour> result = new ArrayList<>();
        String sql = " select a.*, b.name as customer_name, b.phone, c.start_time, d.name as tour_name "
                + "from tbl_customer_tour a "
                + "inner join tbl_customer b on a.customer_id = b.id "
                + "inner join tbl_real_tour c on a.tour_id = c.id "
                + "inner join tbl_tour d on c.tour_id = d.id "
                + "where a.tour_id = ?";
        DbConnection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            connection = MySQLAccess.getInstance().getConn();
            ps = connection.prepareStatement(sql);
            ps.setInt(1, tourId);
            rs = ps.executeQuery();
            while (rs.next()) {
                CustomerTour tour = new CustomerTour();
                tour.setId(AppUtils.parseInt(rs.getObject("id")));
                tour.setCustomer_id(AppUtils.parseInt(rs.getObject("customer_id")));
                tour.setCustomer_name(AppUtils.parseString(rs.getObject("customer_name")));
                tour.setPhone(AppUtils.parseString(rs.getObject("phone")));
                tour.setTour_id(AppUtils.parseInt(rs.getObject("tour_id")));
                tour.setTour_name(AppUtils.parseString(rs.getObject("tour_name")));
                tour.setStatus(AppUtils.parseInt(rs.getObject("status")));
                tour.setRegister_time(rs.getTimestamp("register_time"));
                tour.setStart_time(rs.getTimestamp("start_time"));
                tour.setCreated_time(rs.getTimestamp("created_time"));
                tour.setUpdated_time(rs.getTimestamp("updated_time"));
                result.add(tour);
            }
        } catch (SQLException ex) {
            logger.error("DB error =>", ex);
        } finally {
            MySQLAccess.getInstance().closeResultSet(rs);
            MySQLAccess.getInstance().closePreparedStatement(ps);
            MySQLAccess.getInstance().closeConn(connection);
        }
        return result;
    }

}
