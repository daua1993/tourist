package com.sontx.tourist.db;

import com.kiemanh.vn.DbConnection;

import java.sql.SQLException;

public class DbImpl extends DbConnection {

    public DbImpl(int index, DbConnection conn) throws SQLException {
        super(index, conn.connection());
    }
}
