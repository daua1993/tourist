package com.sontx.tourist.service;

import com.google.gson.Gson;
import com.sontx.tourist.common.AppUtils;
import com.sontx.tourist.db.MySQLActions;
import com.sontx.tourist.entity.CustomerTour;
import com.sontx.tourist.entity.RealTour;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@WebServlet(name = "GetCustomerTour", urlPatterns = {"/GetCustomerTour"})
public class GetCustomerTour extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");

        response.setContentType("application/json");
        int tourId = AppUtils.parseInt(request.getParameter("tour_id"));
        try (PrintWriter out = response.getWriter()) {
            MySQLActions db = new MySQLActions();
            List<CustomerTour> tour = db.getCustomerTour(tourId);
            JSONObject json = new JSONObject();
            JSONArray jArr = new JSONArray();
            if (tour != null & !tour.isEmpty()) {
                for (CustomerTour t : tour) {
                    JSONObject j = new JSONObject();
                    j.put("id", t.getId());
                    j.put("customer_id", t.getCustomer_id());
                    j.put("customer_name", t.getCustomer_name());
                    j.put("phone", t.getPhone());
                    j.put("tour_id", t.getTour_id());
                    j.put("tour_name", t.getTour_name());
                    j.put("status", t.getStatus());
                    j.put("register_time", t.getRegister_time());
                    j.put("start_time", t.getStart_time());
                    j.put("created_time", t.getCreated_time());
                    j.put("updated_time", t.getUpdated_time());
                    jArr.add(j);
                }
            }
            json.put("rows", jArr);
            out.print(new Gson().toJson(json));
            out.flush();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
