package com.sontx.tourist.service;

import com.google.gson.Gson;
import com.sontx.tourist.db.MySQLActions;
import com.sontx.tourist.entity.Customer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

@WebServlet(name = "GetListCustomerService", urlPatterns = {"/GetListCustomerService"})
public class GetListCustomerService extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");

        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            MySQLActions db = new MySQLActions();
            List<Customer> tour = db.getCustomer();
            JSONObject json = new JSONObject();

            json.put("rows", tour);
            out.print(new Gson().toJson(json));
            out.flush();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
