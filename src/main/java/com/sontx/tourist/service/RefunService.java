/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sontx.tourist.service;

import com.google.gson.Gson;
import com.sontx.tourist.common.AppUtils;
import com.sontx.tourist.db.MySQLActions;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author F87
 */
@WebServlet(name = "RefunService", urlPatterns = {"/RefunService"})
public class RefunService extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");

        response.setContentType("application/json");
        int ticketId = AppUtils.parseInt(request.getParameter("ticket_id"));
        try (PrintWriter out = response.getWriter()) {
            MySQLActions db = new MySQLActions();
            int result = db.refunTicket(ticketId);
            JSONObject json = new JSONObject();
            if (result == 0) {
                json.put("result", 0);
                json.put("detail", "OK");
            } else {
                json.put("result", -1);
                json.put("detail", "Trả vé thất bại, vui lòng thừ lại");
            }
            out.print(new Gson().toJson(json));
            out.flush();
        }
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
