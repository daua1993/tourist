package com.sontx.tourist.service;

import com.google.gson.Gson;
import com.sontx.tourist.db.MySQLActions;
import com.sontx.tourist.entity.Tour;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@WebServlet(name = "GetListTourCatService", urlPatterns = {"/GetListTourCatService"})
public class GetListTourCatService extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");

        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            MySQLActions db = new MySQLActions();
            List<Tour> tour = db.getListTour();
            JSONObject json = new JSONObject();
            JSONArray jArr = new JSONArray();
            if (tour != null & !tour.isEmpty()) {
                for (Tour t : tour) {
                    JSONObject j = new JSONObject();
                    j.put("id", t.getId());
                    j.put("name", t.getName());
                    j.put("start_location", t.getStartLocation());
                    j.put("end_location", t.getEndLocation());
                    j.put("update_time", t.getUpdatedTime());
                    jArr.add(j);
                }
            }
            json.put("rows", jArr);
            out.print(new Gson().toJson(json));
            out.flush();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
