package com.sontx.tourist.service;

import com.google.gson.Gson;
import com.sontx.tourist.common.AppUtils;
import com.sontx.tourist.db.MySQLActions;
import com.sontx.tourist.entity.CustomerTourDetail;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author F87
 */
@WebServlet(name = "GetRefunTicketService", urlPatterns = {"/GetRefunTicketService"})
public class GetRefunTicketService extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");

        response.setContentType("application/json");
        int ticketId = AppUtils.parseInt(request.getParameter("ticket_id"));
        try (PrintWriter out = response.getWriter()) {
            MySQLActions db = new MySQLActions();
            CustomerTourDetail tour = db.findOneCustomerTour(ticketId);

            JSONObject json = new JSONObject();
            if (tour != null) {
                long now = System.currentTimeMillis();
                long startTime = tour.getStart_time().getTime();
                int price = tour.getPrice();
                //>7 - 10%
                //>5 - 20
                //>3 - 50
                // 100%

                //1 day = 24*60*60*1000
                int percent = 100;
                if (startTime - now > 7 * 86400000) {
                    percent = 10;
                } else if (startTime - now > 5 * 86400000) {
                    percent = 20;
                } else if (startTime - now > 3 * 86400000) {
                    percent = 50;
                }

                int phat = price / 100 * percent;
                int refun = price - phat;

                json.put("id", tour.getId());
                json.put("customer_id", tour.getCustomer_id());
                json.put("customer_name", tour.getCustomer_name());
                json.put("phone", tour.getPhone());
                json.put("tour_id", tour.getTour_id());
                json.put("tour_name", tour.getTour_name());
                json.put("status", tour.getStatus());
                json.put("register_time", tour.getRegister_time());
                json.put("start_time", tour.getStart_time());
                json.put("created_time", tour.getCreated_time());
                json.put("updated_time", tour.getUpdated_time());
                json.put("locationStart", tour.getLocationStart());
                json.put("locationEnd", tour.getLocationEnd());
                json.put("deputation", tour.getDeputation());
                json.put("price", price);
                json.put("numberOfGuess", tour.getNumberOfGuess());
                json.put("phat", phat);
                json.put("refun", refun);
            }
            out.print(new Gson().toJson(json));
            out.flush();

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
