/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sontx.tourist.common;

/**
 *
 * @author daua1993
 */
public class Common {

    public static final String CONFIG_NAME = "TOURIST";
    public static final String BASE_FOLDER = System.getProperty("user.home") + java.io.File.separator;
    public final static String CONFIG_FOLDER = BASE_FOLDER + "config" + java.io.File.separator + "tourist" + java.io.File.separator;
    public static final String CONFIG_FILE_NAME = "appSetting.xml";
}
