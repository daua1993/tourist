package com.sontx.tourist.common;

import com.google.common.base.Strings;
import com.sontx.tourist.config.AppConfig;

import javax.servlet.http.HttpServletRequest;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AppUtils {

    public static final NumberFormat FORMATER = NumberFormat.getNumberInstance(Locale.GERMAN);

    public static String formatMoney(double vl) {
        return FORMATER.format(vl);
    }

    public static boolean checkIp(HttpServletRequest request) {
        String curIP = request.getHeader("X-FORWARDED-FOR");
        if (curIP == null) {
            curIP = request.getRemoteAddr();
        }
        String acceptedIp = AppConfig.getConfig().getProperty("ACCEPTED_IP", "", "SETTINGS");
        if (acceptedIp == null || acceptedIp.isEmpty()) {
            return true;
        }
        if (acceptedIp.contains(",")) {
            for (String ip : acceptedIp.split(",")) {
                if (ip.equalsIgnoreCase(curIP)) {
                    return true;
                }
            }
        }
        return acceptedIp.equalsIgnoreCase(curIP);
    }

    public static String parseString(Object obj) {
        if (obj == null) {
            return "";
        }
        try {
            return String.valueOf(obj);
        } catch (Exception ex) {
        }
        return "";
    }

    public static int parseInt(Object obj) {
        if (obj == null) {
            return 0;
        }
        try {
            if (obj instanceof Double) {
                return ((Double) obj).intValue();
            }
            if (obj instanceof Float) {
                return ((Float) obj).intValue();
            }
            return Integer.parseInt(String.valueOf(obj));
        } catch (Exception ex) {
        }
        return 0;
    }

    public static long parseLong(Object obj) {
        if (obj == null) {
            return 0;
        }
        try {
            if (obj instanceof Double) {
                return ((Double) obj).longValue();
            }
            if (obj instanceof Float) {
                return ((Float) obj).longValue();
            }
            return Long.parseLong(String.valueOf(obj));
        } catch (Exception ex) {
        }
        return 0;
    }

    public static String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte anArray : array) {
                sb.append(Integer.toHexString((anArray & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static String formatDateToString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return simpleDateFormat.format(date);
    }

    public static Date stringToDate(String date, String pattern) {
        if (Strings.isNullOrEmpty(date)) {
            return new Date();
        }
        Date date1;
        try {
            date1 = new SimpleDateFormat(pattern).parse(date);
        } catch (Exception e) {
            date1 = new Date();
        }
        return date1;
    }

    public static int formatDate(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        return parseInt(simpleDateFormat.format(date));
    }

    public static String formatDate(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public static int findVip(long totalTrans) {
        int vip = 0;
        for (int i = 1; i <= 10; i++) {
            long money = AppConfig.getConfig().getLongProperty("VIP_LEVEL_" + i, 0, "VIP_POINT");
            if (totalTrans >= money) {
                vip = i;
            } else {
                break;
            }
        }
        return vip;
    }

    public static boolean validateUsername(String username) {
        if (Strings.isNullOrEmpty(username) || username.trim().length() < 4 || username.trim().length() > 15) {
            return false;
        }
        return username.matches("[A-Za-z0-9_]+");
    }

}
