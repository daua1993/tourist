/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sontx.tourist.entity;

/**
 *
 * @author F87
 */
public class CustomerTourDetail extends CustomerTour {
    
    private String locationStart;
    private String locationEnd;
    private String deputation;
    private int price;
    private int numberOfGuess;

    public String getLocationStart() {
        return locationStart;
    }

    public void setLocationStart(String locationStart) {
        this.locationStart = locationStart;
    }

    public String getLocationEnd() {
        return locationEnd;
    }

    public void setLocationEnd(String locationEnd) {
        this.locationEnd = locationEnd;
    }

    public String getDeputation() {
        return deputation;
    }

    public void setDeputation(String deputation) {
        this.deputation = deputation;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getNumberOfGuess() {
        return numberOfGuess;
    }

    public void setNumberOfGuess(int numberOfGuess) {
        this.numberOfGuess = numberOfGuess;
    }

    @Override
    public String toString() {
        return "CustomerTourDetail{" + "locationStart=" + locationStart + ", locationEnd=" + locationEnd + ", deputation=" + deputation + ", price=" + price + ", numberOfGuess=" + numberOfGuess + '}';
    }
    
}
