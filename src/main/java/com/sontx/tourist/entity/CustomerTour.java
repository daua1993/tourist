/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sontx.tourist.entity;

import java.sql.Timestamp;

/**
 *
 * @author F87
 */
public class CustomerTour {
    
    private int id;
    private int customer_id;
    private String customer_name;
    private String phone;
    private int tour_id;
    private String tour_name;
    private int status;
    private Timestamp start_time;
    private Timestamp register_time;
    private Timestamp created_time;
    private Timestamp updated_time;

    public Timestamp getStart_time() {
        return start_time;
    }

    public void setStart_time(Timestamp start_time) {
        this.start_time = start_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getTour_id() {
        return tour_id;
    }

    public void setTour_id(int tour_id) {
        this.tour_id = tour_id;
    }

    public String getTour_name() {
        return tour_name;
    }

    public void setTour_name(String tour_name) {
        this.tour_name = tour_name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getRegister_time() {
        return register_time;
    }

    public void setRegister_time(Timestamp register_time) {
        this.register_time = register_time;
    }

    public Timestamp getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Timestamp created_time) {
        this.created_time = created_time;
    }

    public Timestamp getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Timestamp updated_time) {
        this.updated_time = updated_time;
    }

    @Override
    public String toString() {
        return "CustomerTour{" + "id=" + id + ", customer_id=" + customer_id + ", customer_name=" + customer_name + ", phone=" + phone + ", tour_id=" + tour_id + ", tour_name=" + tour_name + ", status=" + status + ", register_time=" + register_time + ", created_time=" + created_time + ", updated_time=" + updated_time + '}';
    }

}
