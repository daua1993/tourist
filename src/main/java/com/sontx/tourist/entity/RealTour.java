/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sontx.tourist.entity;

import java.sql.Timestamp;

/**
 *
 * @author F87
 */
public class RealTour {

    private int id;
    private int tourId;
    private String name;
    private String deputation;
    private int price;
    private int paid;
    private int numOfCustomer;
    private Timestamp startTime;
    private String description;
    private Timestamp createdTime;
    private Timestamp updatedTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTourId() {
        return tourId;
    }

    public void setTourId(int tourId) {
        this.tourId = tourId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeputation() {
        return deputation;
    }

    public void setDeputation(String deputation) {
        this.deputation = deputation;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPaid() {
        return paid;
    }

    public void setPaid(int paid) {
        this.paid = paid;
    }

    public int getNumOfCustomer() {
        return numOfCustomer;
    }

    public void setNumOfCustomer(int numOfCustomer) {
        this.numOfCustomer = numOfCustomer;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    public Timestamp getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Timestamp updatedTime) {
        this.updatedTime = updatedTime;
    }

    @Override
    public String toString() {
        return "RealTour{" + "id=" + id + ", tourId=" + tourId + ", name=" + name + ", deputation=" + deputation + ", price=" + price + ", paid=" + paid + ", numOfCustomer=" + numOfCustomer + ", startTime=" + startTime + ", description=" + description + ", createdTime=" + createdTime + ", updatedTime=" + updatedTime + '}';
    }

}
