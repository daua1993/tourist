/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sontx;

import com.sontx.tourist.common.Common;
import com.sontx.tourist.config.AppConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.logging.log4j.core.config.Configurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author daua1993
 */

public class Main extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();

        //Load config log4j
//        Configurator.initialize(null, Common.CONFIG_FOLDER + "log4j2.xml");
        Logger logger = LoggerFactory.getLogger(this.getClass());
        try {
            logger.info("server inited !!");
            logger.info("===============!");
            logger.info("init server !!!!");
            logger.info("CONFIG_FOLDER: = {}", Common.CONFIG_FOLDER);

            // Load Config
            AppConfig appConfig = new AppConfig();
            appConfig.reloadConfig();

            //Load role
//            Permission pms = Permission.getInstance();
//            pms.init();
//            CommonBankTransaction.getInstance();
        } catch (Exception ex) {
            logger.error("Load Config Failed {}", ex);
        }
    }

}

